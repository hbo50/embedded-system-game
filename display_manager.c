/**
 @file display_manager.c
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief Display module for the Rock, Paper, Scissors game on an UCFUNKIT.
 This module offers functions for initializing the display, clearing the display content,
 displaying individual characters, scrolling text messages, and updating the display.
 */

#include "display_manager.h"

/**
 * Initializes the display with specified rates.
 * 
 * Sets up the display settings including font, pacer rate, and message speed for
 * the game. Ensure this function is called first to set up the display system correctly.
 * 
 * @param pacer_rate: Rate at which the display updates.
 * @param message_rate: Speed at which text messages scroll on the display.
 */
void display_manager_init(uint16_t pacer_rate, uint8_t message_rate)
{
    tinygl_init(pacer_rate);
    tinygl_font_set(&font5x7_1);
    tinygl_text_speed_set(message_rate);
}

/**
 * Clears any existing content from the display.
 * 
 * Any characters or messages currently on the screen will be removed, leaving a blank display.
 */
void display_manager_clear(void) {
    tinygl_clear();
}

/**
 * Shows a specific character on the display.
 * 
 * The selected character will be displayed, replacing any previous content on the screen.
 * 
 * @param character: Character to be shown.
 */
void display_manager_character(char character) {
    char buffer[2];
    buffer[0] = character;
    buffer[1] = '\0';
    tinygl_text(buffer);
}

/**
 * Scrolls a string message across the display.
 * 
 * Presents the given string as scrolling text, replacing any prior content.
 * 
 * @param message: The string message for display.
 */
void display_manager_message(const char* message) {
    tinygl_text(message);
}

/**
 * Regularly updates the display to reflect current content.
 * 
 * Call this function periodically in the main game loop to ensure a smooth and
 * consistent display output.
 */
void display_manager_update(void) {
    tinygl_update();
}
