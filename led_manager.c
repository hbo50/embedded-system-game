/**
 @file led_manager.c
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief LED module for the Rock, Paper, Scissors game on an UCFUNKIT.
 This module provides functions for initializing, turning on, and turning off the LED.
 */

#include "led_manager.h"

/**
 * Initializes the LED system.
 */
void led_manager_init(void)
{
    pio_config_set(LED1_PIO, PIO_OUTPUT_LOW);
}

/**
 * Turns the LED on.
 */
void led_manager_on(void)
{
    pio_output_high(LED1_PIO);
}

/**
 * Turns the LED off.
 */
void led_manager_off(void)
{
    pio_output_low(LED1_PIO);
}
