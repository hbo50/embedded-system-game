/**
 @file communication.h
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date [Date, e.g. 10/16/23]
 @brief Header file defining the communication interface for Rock, Paper, Scissors on an UCFUNKIT.
 This file contains the prototypes for the functions that handle IR communication, 
 including sending, receiving, and initializing the communication module.
 */

#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <stdbool.h>

/**
 * Initialize communication module.
 */
void communication_init(void);

/**
 * Sends the provided choice (Rock, Paper, or Scissors) via IR communication.
 * 
 * @param choice - The game choice ('R', 'P', or 'S') to send.
 */
void communication_send(char choice);

/**
 * Receives and validates a choice (Rock, Paper, or Scissors) via IR communication.
 * 
 * @return The received choice if valid ('R', 'P', or 'S'), 
 *         or a null character ('\0') if no valid choice is received.
 */
char communication_receive(void);

#endif // COMMUNICATION_H
