/**
 @file display_manager.h
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief Provides display functionality for the Rock, Paper, Scissors game on UCFUNKIT.
 This module offers methods for initializing the display, clearing the screen,
 displaying a single character, showcasing a string message, and regularly updating
 the display content.
 */

#ifndef DISPLAY_MANAGER_H
#define DISPLAY_MANAGER_H

#include "tinygl.h"
#include "../fonts/font5x7_1.h"

/**
 * Initializes the display settings.
 */
void display_manager_init(uint16_t pacer_rate, uint8_t message_rate);

/**
 * Clears the display screen.
 */
void display_manager_clear(void);

/**
 * Displays a single character on the screen.
 *
 * @param character The character to be displayed.
 */
void display_manager_character(char character);

/**
 * Displays a string on the screen.
 *
 * @param message The string message to be displayed.
 */
void display_manager_message(const char* message);

/**
 * Regular update for display.
 */
void display_manager_update(void);

#endif // DISPLAY_MANAGER_H
