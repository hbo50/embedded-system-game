/**
 @file game_logic.c
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief Logic module for the Rock, Paper, Scissors game on an UCFUNKIT.
 This module contains functions to determine the result of a Rock, Paper, Scissors 
 game round based on the choices of the two players.
 */

#include "game_logic.h"

/**
 * Determines the result of a Rock, Paper, Scissors game based on the choices of two players.
 * 
 * @param player1_choice - The choice ('R', 'P', or 'S') made by Player 1.
 * @param player2_choice - The choice ('R', 'P', or 'S') made by Player 2.
 * 
 * @return "Draw!" if both players made the same choice.
 * @return "Win!" if Player 1's choice wins over Player 2's choice.
 * @return "Lose!" if Player 1's choice loses to Player 2's choice.
 */
const char* determine_result(char player1_choice, char player2_choice)
{   
    // Check for draw
    if (player1_choice == player2_choice) {

        return "Draw!";
    }
    // Check Winning condition
    else if ((player1_choice == 'R' && player2_choice == 'S') ||
             (player1_choice == 'P' && player2_choice == 'R') ||
             (player1_choice == 'S' && player2_choice == 'P')) {

        return "Win!";
    }
    // Other loss
    else {
        return "Lose!";
    }
}
