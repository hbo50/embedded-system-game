/**
 @file led_manager.h
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief LED management module for the Rock, Paper, Scissors game on an UCFUNKIT.
 This module provides functions for initializing the LED, setting the LED state to on or off,
 and managing any other LED-related functionalities. It abstracts direct hardware calls 
 ensuring the main game logic is decoupled from direct LED operations.
 */

#ifndef LED_MANAGER_H
#define LED_MANAGER_H

#include "pio.h"

/**
 * Initializes the LED system.
 */
void led_manager_init(void);

/**
 * Turns the LED on.
 */
void led_manager_on(void);

/**
 * Turns the LED off.
 */
void led_manager_off(void);

#endif // LED_MANAGER_H
