
# Rock Paper Scissors for ENCE260 project

A Rock Paper Scissors game made to run on two UC Funkitss. The game uses IR transmissions to communicate between the two funkits


## Authors

- [Henry James Borthwick (@hbo50)](https://eng-git.canterbury.ac.nz/hbo50)
- [Gimhan Dissanayaka Mudiyanselage (@gdi31)](https://eng-git.canterbury.ac.nz/gdi31)


## Installation

1: Clone this repository:
```bash
git clone https://eng-git.canterbury.ac.nz/ence260-2023/group_321
```
2: Navigate to the project folder:
```bash
cd group_321
```
3: Compile the program by running the makefile
```bash
make
```
4: Send and run the program on both the UC Funkits by running the following command twice
```bash
make program
```
## Gameplay
- For those unfamiliar with Rock, Paper, Scissors:
    - Rock crushes Scissors
    - Scissors cuts Paper
    - Paper covers Rock

- Use the navigation stick to choose an option out of Rock ("R"), Paper ("P") or Scissors("S")
- Press the navigation stick down to confirm your choice
- After making your choice, the device will wait for your opponent to make their choice
- Once both players have made their choices, the funkits will communicate and display the winner.

- Want to play again? Simply move the navigation pad up or down again and you're ready for another round.