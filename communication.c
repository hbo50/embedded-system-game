/**
 @file communication.c
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief Communication module for the Rock, Paper, Scissors game on an UCFUNKIT.
 This module provides functions for initializing IR communication, sending a game choice 
 (Rock, Paper, Scissors) via infrared, and receiving a valid game choice from another player.
 */

#include "communication.h"
#include "ir_uart.h"
#include "pio.h"

/**
 * Initializes the communication module by setting up the IR driver.
 */
void communication_init(void)
{
    // Initialize IR driver
    ir_uart_init();
}

/**
 * Sends the provided choice (Rock, Paper, or Scissors) via IR communication.
 * 
 * @param choice - The game choice ('R', 'P', or 'S') to send.
 */
void communication_send(char choice)
{
    ir_uart_putc(choice);
}

/**
 * Receives and validates a choice (Rock, Paper, or Scissors) via IR communication.
 * 
 * @return The received choice if valid ('R', 'P', or 'S'), 
 *         or a null character ('\0') if no valid choice is received.
 */
char communication_receive(void)
{
    if (ir_uart_read_ready_p()) {
        // Get other players input
        char received_choice = ir_uart_getc();

        // Check recived input is correct
        if (received_choice == 'R' || received_choice == 'P' || received_choice == 'S') {
            // Shows player choice recived
            pio_output_high(LED1_PIO);
            return received_choice;
        }
    }

    return '\0';
}
