/**
 @file game.c
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date 10/16/23
 @brief Main driver for the Rock, Paper, Scissors game on an UCFUNKIT.
 The game allows players to cycle through and select an option (Rock, Paper, Scissors).
 Upon selection, their choice is sent via infrared to the other player. 
 After both players make their choice, the outcome is displayed: Win, Lose, or Draw.
 */

#include "system.h"
#include "pacer.h"
#include "navswitch.h"
#include <stdbool.h>

#include "communication.h"
#include "game_logic.h"
#include "display_manager.h"
#include "led_manager.h"

// Define global constants
#define PACER_RATE 500
#define MESSAGE_RATE 10

/**
 * Main function to execute the Rock, Paper, Scissors game on an UCFUNKIT.
 * Players can cycle through and select an option. Upon selection, their choice
 * is sent via infrared to the other player. After both players make their choice,
 * the outcome is displayed: Win, Lose, or Draw.
 */
int main(void)
{
    // Define the game characters: Rock (R), Paper (P), Scissors (S)
    char character[] = {'R', 'P', 'S'};

    // Counter for navigating through the game character selections
    int8_t i = 0;

    // Default choices for both players
    char player1_choice = character[0];
    char player2_choice = character[0];

    // Flags to track infrared communication status
    bool recieved = false;
    bool sent = false;

    // Initialize system components
    system_init();
    navswitch_init();
    pacer_init(PACER_RATE);

    // Initialize display with display_manager.c
    display_manager_init(PACER_RATE, MESSAGE_RATE);

    // Initialize display with communication.c
    communication_init();

    // Initialize LED with led_manager.c
    led_manager_init();
    
    // Use display manager to show welcome screen
    display_manager_message("|RPS"); 

    // Main game loop
    while (1) {
        // Regular system updates
        pacer_wait();
        navswitch_update();
        display_manager_update(); 

        // If we haven't already received a valid choice, check for a new one
        if (!recieved) {
            // Use the communication module to check for incoming IR messages
            player2_choice = communication_receive();

            // Double check to ensure valid input
            if (player2_choice == 'R' || player2_choice == 'P' || player2_choice == 'S') {
                recieved = true;
            }
        }

        // Navigate through R, P, S using the North, South, East, and West switches
        if (navswitch_push_event_p(NAVSWITCH_NORTH) || navswitch_push_event_p(NAVSWITCH_EAST)) {
            i = (i + 1) % 3;
            // Update the displayed character based on selection
            display_manager_character(character[i]);
        } else if (navswitch_push_event_p(NAVSWITCH_SOUTH) || navswitch_push_event_p(NAVSWITCH_WEST)) {
            i = (i - 1 + 3) % 3;
            // Update the displayed character based on selection
            display_manager_character(character[i]);
        }

        // Make a selection and transmit choice over IR when pushed down
        if (navswitch_push_event_p(NAVSWITCH_PUSH)) {
            // Store opponent choice
            player1_choice = character[i];
            
            // Use the communication module to send the choice
            communication_send(player1_choice); 

            sent = true;

            // Use display manager to clear display
            display_manager_clear();
        }

        // Evaluate the game's outcome once both players have made their choice
        while (recieved && sent) {
            // Update display with display manager
            display_manager_update();

            // Use the game_logic module to determine the game result
            const char* result = determine_result(player1_choice, player2_choice);

            // Use display manager to show result message
            display_manager_message(result); 

            // Reset for the next game round
            recieved = false;
            sent = false;
            led_manager_off();
        }
    }

    // End of main function
    return 0;
}
