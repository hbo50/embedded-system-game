/**
 @file game_logic.h
 @author Henry James Borthwick, hbo50 & Gimhan Dissanayaka Mudiyanselage, gdi31
 @date [Date, e.g. 10/16/23]
 @brief Header file defining the game logic for Rock, Paper, Scissors on an UCFUNKIT.
 This file contains the prototype for the function that determines the result of the game 
 based on the choices of two players.
 */

#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

/**
 * Determines the result of a Rock, Paper, Scissors game based on the choices of two players.
 * 
 * @param player1_choice - The choice ('R', 'P', or 'S') made by Player 1.
 * @param player2_choice - The choice ('R', 'P', or 'S') made by Player 2.
 * 
 * @return "Draw!" if both players made the same choice.
 * @return "Win!" if Player 1's choice wins over Player 2's choice.
 * @return "Lose!" if Player 1's choice loses to Player 2's choice.
 */
const char* determine_result(char player1_choice, char player2_choice);

#endif // GAME_LOGIC_H
